//
//  GridL2ViewModel.swift
//  Mind Game
//
//  Created by SRBD on 11/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import Foundation

class GridL2ViewModel {
    
    var numArray = [Int]()
    
    init() {
        prepareData()
    }
    
    private func prepareData() {
        numArray = Array(0..<84).shuffled()
    }
}

extension GridL2ViewModel {
    var numberOfItems: Int {
        return 84
    }
    
    var cellPerRow: Int {
        return 7
    }
    
    func getSuffledNum(indexPath: IndexPath) -> String {
        return String(numArray[indexPath.row])
    }
}
