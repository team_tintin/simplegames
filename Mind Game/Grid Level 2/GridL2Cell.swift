//
//  GridL2Cell.swift
//  Mind Game
//
//  Created by SRBD on 11/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridL2Cell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    static var reuseId: String {
        return String(describing: GridL2Cell.classForCoder())
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: nil)
    }
}

