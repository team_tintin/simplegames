//
//  GridL2Viewcontroller.swift
//  Mind Game
//
//  Created by SRBD on 11/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridL2Viewcontroller: UIViewController {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    private let spaceBetweenCells: CGFloat = 10
    var viewModel: GridL2ViewModel!
    var incresingNum: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressView()
        gridCollectionView.register(GridL2Cell.nib, forCellWithReuseIdentifier: GridL2Cell.reuseId)
        viewModel = GridL2ViewModel()
        gridCollectionView.dataSource = self
        gridCollectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        incresingNum = 0
    }
    
    private func showAlertBox() {
        let title = "Wrong"
        let message = "Try Again"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.incresingNum = 0
            self.gridCollectionView.reloadData()
        })
        
        alertController.addAction(action)
        self.present(alertController, animated: true)
    }
    
    private func setProgressView() {
        
        let progress = Progress(totalUnitCount: 100)
        progressView.setProgress(0, animated: true)
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            
            guard progress.isFinished == false else {
                timer.invalidate()
                return
            }
            
            progress.completedUnitCount += 1
            self.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
            
        }
    }
    
}

extension GridL2Viewcontroller: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = gridCollectionView.dequeueReusableCell(withReuseIdentifier: GridL2Cell.reuseId,
                                                          for: indexPath) as! GridL2Cell
        if Int(viewModel.getSuffledNum(indexPath: indexPath))! < incresingNum {
            cell.nameLabel.text = ""
            return cell
        }
        cell.nameLabel.text = viewModel.getSuffledNum(indexPath: indexPath)
        cell.backgroundColor = UIColor.lightGray
        return cell
    }
}

extension GridL2Viewcontroller: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let noGapWidth = width - spaceBetweenCells * CGFloat((viewModel.cellPerRow + 1))
        let cellWidth = noGapWidth/CGFloat(viewModel.cellPerRow)
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spaceBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spaceBetweenCells, left: spaceBetweenCells, bottom: spaceBetweenCells, right: spaceBetweenCells)
    }
}

extension GridL2Viewcontroller: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedInt = Int(viewModel.getSuffledNum(indexPath: indexPath))!
        guard selectedInt >= incresingNum else { return }
        if selectedInt == incresingNum {
            incresingNum = incresingNum + 1
            gridCollectionView.reloadData()
        } else {
            showAlertBox()
        }
    }
}
