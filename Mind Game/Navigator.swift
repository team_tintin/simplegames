//
//  NavigationController.swift
//  Mind Game
//
//  Created by SRBD on 18/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import Foundation
import UIKit

enum Destination: CaseIterable {
    case Level1
    case Level2
    case Level3
}

class Navigator {
    
    private var rootViewController: UIViewController
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    private func makeViewController(destination: Destination) -> UIViewController {
        
        switch destination {
        case .Level1:
            let storyBoard = UIStoryboard(name: "Grid", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "GridViewController") as! GridViewController
            return viewController
        case .Level2:
            let storyBoard = UIStoryboard(name: "GridL2", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "GridL2Viewcontroller") as! GridL2Viewcontroller
            return viewController
        case .Level3:
            let storyBoard = UIStoryboard(name: "GridL3", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "GridL3ViewController") as! GridL3ViewController
            return viewController
        }
    }
    
    func pushViewController(destination: Destination) {
        let vc = makeViewController(destination: destination)
        rootViewController.navigationController?.pushViewController(vc, animated: true)
    }
}
