
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var destination: Destination = .Level1
    var contentOffSet: CGFloat = 0
    var index: Int = 0
    var bgImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.bounces = false
        headerTitle.text = "Level \(index + 1)"
        prepareImages()
        collectionView.register(UINib(nibName: SliderCell.reuseId,
                                      bundle: nil),
                                forCellWithReuseIdentifier: SliderCell.reuseId)
    }
    
    @IBAction func startButtonTapped(_ sender: UIButton) {
        let navigator = Navigator(rootViewController: self)
        navigator.pushViewController(destination: destination)
    }
    
    private func prepareImages() {
        for i in 1..<4 {
            bgImages.append(UIImage(named: "Level\(i)")!)
        }
        
        bgImages = bgImages.map({
            return imageWithImage(image: $0, scaledToSize: collectionView.frame.size)
        })
        
        print(bgImages.count)
        
    }
    
    private func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension ViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCell.reuseId, for: indexPath) as! SliderCell
        cell.bgImageView.image = bgImages[indexPath.row]
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return view.bounds.size
    }
}

extension ViewController: UICollectionViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if contentOffSet > scrollView.contentOffset.x {
            index = max(index - 1, 0)
            destination = Destination.allCases[index]
            headerTitle.text = "Level \(index + 1)"
            contentOffSet = scrollView.contentOffset.x
        } else if contentOffSet < scrollView.contentOffset.x{
            index = min(index + 1, 2)
            destination = Destination.allCases[index]
            headerTitle.text = "Level \(index + 1)"
            contentOffSet = scrollView.contentOffset.x
        }
    }
}



