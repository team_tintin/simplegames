
import UIKit

class SliderCell: UICollectionViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.red
    }
    
    static var reuseId: String {
        return String(describing: SliderCell.classForCoder())
    }
}
