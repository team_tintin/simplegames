import Foundation

struct CharSet {
    let first: String
    let second: String

}

class GridViewModel {
    private var charTuples = [CharSet]()
    init() {
     prepareData()
    }
    
    func prepareData() {
        charTuples.append(CharSet(first: "7", second: "1"))
        charTuples.append(CharSet(first: "3", second: "8"))
        charTuples.append(CharSet(first: "Q", second: "0"))
    }
}

extension GridViewModel {
    var numberOfItems: Int {
        return 84
    }
    
    var cellPerRow: Int {
        return 7
    }
    
    var charSet: CharSet {
        return charTuples[0]
    }
}
