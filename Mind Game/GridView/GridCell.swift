//
//  GridCell.swift
//  Mind Game
//
//  Created by SRBD on 7/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridCell: UICollectionViewCell {
    
    @IBOutlet weak var charLabel: UILabel!
    static var reuseId: String {
        return String(describing: GridCell.classForCoder())
    }
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: nil)
    }
}
