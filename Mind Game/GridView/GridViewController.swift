//
//  GridViewController.swift
//  Mind Game
//
//  Created by SRBD on 7/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridViewController: UIViewController {
    
    @IBOutlet weak var gridCollectionView: UICollectionView!
    private let spaceBetweenCells: CGFloat = 10
    var viewModel: GridViewModel!
    @IBOutlet weak var progressView: UIProgressView!
    var cellIndexForOneNum: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = GridViewModel()
        cellIndexForOneNum = Int.random(in: 0..<viewModel.numberOfItems)
        setProgressView()
        gridCollectionView.register(GridCell.nib, forCellWithReuseIdentifier: GridCell.reuseId)
        gridCollectionView.dataSource = self
        gridCollectionView.delegate  = self
    }
    
    
    private func setProgressView() {
        
        let progress = Progress(totalUnitCount: 30)
        progressView.setProgress(0, animated: true)
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            
            guard progress.isFinished == false else {
                timer.invalidate()
                return
            }
            
            progress.completedUnitCount += 1
            self.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
            
        }
    }
    
    private func showAlertBox(isCorrectCell: Bool) {
        let title = isCorrectCell ? "Right" : "Wrong"
        let message = isCorrectCell ? "Congratulations" : "Try Again"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.cellIndexForOneNum = Int.random(in: 0..<self.viewModel.numberOfItems)
            self.gridCollectionView.reloadData()
        })
        
        alertController.addAction(action)
        self.present(alertController, animated: true)
    }
}

extension GridViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = gridCollectionView.dequeueReusableCell(withReuseIdentifier: GridCell.reuseId, for: indexPath) as? GridCell else {
            return UICollectionViewCell()
        }
        
        if indexPath.row != cellIndexForOneNum {
            cell.charLabel.text = viewModel.charSet.first
        } else {
            cell.charLabel.text = viewModel.charSet.second
        }
        
        cell.backgroundColor = UIColor.gray
        return cell
    }
}

extension GridViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showAlertBox(isCorrectCell: indexPath.row == cellIndexForOneNum)
    }
}

extension GridViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let noGapWidth = width - spaceBetweenCells * CGFloat((viewModel.cellPerRow + 1))
        let cellWidth = noGapWidth/CGFloat(viewModel.cellPerRow)
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spaceBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spaceBetweenCells, left: spaceBetweenCells, bottom: spaceBetweenCells, right: spaceBetweenCells)
    }
}
