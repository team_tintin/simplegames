//
//  Grid3ViewModel.swift
//  Mind Game
//
//  Created by SRBD on 13/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import Foundation

class Grid3ViewModel {
    
    var storedArray = [Int]()
    
    init() {
        prepareData()
    }
    
    func prepareData() {
        let rand1 = Array(1..<9).shuffled()
        let rand2 = Array(1..<9).shuffled()
        storedArray = rand1 + rand2
    }
}

