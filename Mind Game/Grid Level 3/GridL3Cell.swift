//
//  GridL3Cell.swift
//  Mind Game
//
//  Created by SRBD on 13/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridL3Cell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var desView: UIView!
    
    var cellState: Bool = false
    var firstTime: Bool = true
    
    static var reuseId: String {
        return String(describing: GridL3Cell.classForCoder())
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    func configure(state: Bool?, image: UIImage?, text: String) {
        
        if let state = state {
            imageView.image = image
            desLabel.text = text
            if state {
                desView.isHidden = false
                flip()
            } else {
                desView.isHidden = true
                flipBack()
            }
        }
    }
    
    
    func flip() {
        UIView.transition(from: desView, to: imageView, duration: 0.3, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
    }
    
    func flipBack() {
        UIView.transition(from: imageView, to: desView, duration: 0.3, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
    }
    
}
