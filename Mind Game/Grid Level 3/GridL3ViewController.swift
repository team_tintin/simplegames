//
//  GridL3ViewController.swift
//  Mind Game
//
//  Created by SRBD on 13/7/20.
//  Copyright © 2020 SRBD. All rights reserved.
//

import UIKit

class GridL3ViewController: UIViewController {

    @IBOutlet weak var gridCollectionView: UICollectionView!
    @IBOutlet weak var progressBar: UIProgressView!
    private var viewModel: Grid3ViewModel!
    var toggle: Bool = true
    var savedIndex1: IndexPath = IndexPath(row: -1, section: 0)
    var savedIndex2: IndexPath = IndexPath(row: -1, section: 0)
    var l1: Int = -1
    var l2: Int = -1
    var shouldHide: Bool = true
    var state: Bool = true
    var totalCOunt = 16
    
    private let spaceBetweenCells: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = Grid3ViewModel()
        progressBar.progress = 0
        setupCollectionView()
        setProgressView()
    }
    
    private func setProgressView() {
        
        let progress = Progress(totalUnitCount: 100)
        progressBar.setProgress(0, animated: true)
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            
            guard progress.isFinished == false else {
                timer.invalidate()
                return
            }
            
            progress.completedUnitCount += 1
            self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
            
        }
    }
    
    private func setupCollectionView() {
        gridCollectionView.dataSource = self
        gridCollectionView.delegate = self
        
        gridCollectionView.register(GridL3Cell.nib, forCellWithReuseIdentifier: GridL3Cell.reuseId)
        gridCollectionView.isUserInteractionEnabled = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.state = !self.state
            self.gridCollectionView.isUserInteractionEnabled = true
            self.gridCollectionView.reloadData()
        }
    }
    
}

extension GridL3ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = gridCollectionView.dequeueReusableCell(withReuseIdentifier: GridL3Cell.reuseId, for: indexPath) as! GridL3Cell
        let image =  UIImage(named: "colour\(viewModel.storedArray[indexPath.row])")
        let text = "Tap to see hidden image"
        cell.configure(state: state, image: image, text: text)
        cell.backgroundColor = UIColor.lightGray
        cell.alpha = 0
        return cell
    }
}

extension GridL3ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let noGapWidth = width - spaceBetweenCells * CGFloat((4 + 1))
        let cellWidth = noGapWidth/CGFloat(4)
        
        return CGSize(width: cellWidth, height: 130.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spaceBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spaceBetweenCells, left: spaceBetweenCells, bottom: spaceBetweenCells, right: spaceBetweenCells)
    }
}

extension GridL3ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = gridCollectionView.cellForItem(at: indexPath) as! GridL3Cell
        
        if totalCOunt == 1 {
            
        }
        
        if cell.cellState { return }
        
        if totalCOunt % 2 == 0 {
            savedIndex1 = indexPath
            l1 = viewModel.storedArray[indexPath.row]
            cell.flip()
            totalCOunt = totalCOunt - 1
            if savedIndex1 == savedIndex2 {
                
            }
        } else {
            savedIndex2 = indexPath
            l2 = viewModel.storedArray[indexPath.row]
            
            if (savedIndex2 == savedIndex1) && (l1 == l2) {
                cell.flipBack()
                totalCOunt = totalCOunt + 1
            } else if (l1 == l2) && (savedIndex2 != savedIndex1) {
                let cell1 = collectionView.cellForItem(at: savedIndex1) as! GridL3Cell
                let cell2 = collectionView.cellForItem(at: savedIndex2) as! GridL3Cell
                cell1.cellState = true
                cell2.cellState = true
                cell2.flip()
                totalCOunt = totalCOunt - 1
            } else {
                let cell1 = collectionView.cellForItem(at: savedIndex1) as! GridL3Cell
                cell.flip()
                totalCOunt = totalCOunt + 1
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    cell.flipBack()
                    cell1.flipBack()
                }
            }
        }
    }
}
